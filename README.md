# texlive2021

EasyBuild easyconfig file for installing TexLive 2021

## Getting started

No easyconfig file available for the latest TexLive 2021 version on the official GitHub repo for EasyBuild.
One may want to use the eb file in this repo instead, albeit after making necessary changes wrt toolchain and version of choice for dependencies.

## Steps to install TexLive 2021 using EasyBuild

#### Prerequisites
    - Python >= 3.8
	- PIP
    - Environment-Modules ([Reference](https://modules.readthedocs.io/en/stable/INSTALL.html))
    - EasyBuild 4.4.2 ([Docs](https://docs.easybuild.io/en/latest/Installation.html))

#### EasyBuild Usage:
    	- eb -S 'pattern/keyword/wildcards' [to search easyconfigs for a given software]
		- eb --show-ec easyconfig.eb [Inspect easyconfig file]
		- eb -D easyconfig.eb [does a short dry-run of the given easyconfig file]
		- eb -M easyconfig.eb [shows what dependencies of the given easyconfig file are missing in the current system]
		- eb -x easyconfig.eb [shows an overview of the installation procedure of the easyconfig file steps]
		- eb easyconfig.eb [Installs the software specified in the easyconfig file]

        NOTE: Add "--robot" to the arguments of "eb". It will then download and install all missing dependencies
